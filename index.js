
function getLogin() {
    const firstNameLetter = this.firstName[0].toLocaleLowerCase();
    const surnameInLowerCase = this.lastName.toLocaleLowerCase();

    return `${firstNameLetter}${surnameInLowerCase}`;
}

function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your last name?');

    const newUser = { firstName, lastName };

    newUser.getLogin = getLogin;

    return newUser;
}

const user1 = createNewUser();
const user1Login = user1.getLogin();

console.log(user1Login);
